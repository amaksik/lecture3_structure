﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DAL.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int? ProjectId { get; set; }
        public Project Project { get; set; }
        public int? PerfomerId { get; set; }
        public User Perfomer { get; set; }
    }
}
