using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectStructure.DAL.Context;
using ProjectStructure.BL.Services;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.MapperProfiles;
using ProjectStructure.BLL.Unit_of_Work.Interfaces;
using ProjectStructure.BLL.Unit_of_Work.Classes;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace ProjectStructure.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<ProjectStructureContext>(opt => opt.UseInMemoryDatabase(databaseName: "InMemoryDb"),
                ServiceLifetime.Singleton, ServiceLifetime.Singleton);

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IQueriesService, QueriesService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            
            
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ModelsProfile>();
                mc.AddProfile<LastProjectCountAndLongerTasksProfile>();
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
