﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Unit_of_Work.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<T> Set<T>() where T : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();

    }
}
