﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Unit_of_Work.Interfaces
{
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> Get();
        void Create(T entity);
        void Update(T entity);
        void Delete(object id);
        void Delete(T entity);
    }
}
