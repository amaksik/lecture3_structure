﻿using ProjectStructure.BLL.Unit_of_Work.Interfaces;
using ProjectStructure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Unit_of_Work.Classes
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectStructureContext _context;

        public UnitOfWork(ProjectStructureContext context)
        {
            _context = context;
            GetContext.Initialize(_context);
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public IRepository<T> Set<T>() where T : class
        {
            return new Repository<T>(_context);
        }
    }
}
