﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Exceptions;
using ProjectStructure.BLL.Unit_of_Work.Interfaces;
using ProjectStructure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Unit_of_Work.Classes
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private ProjectStructureContext _context;
        public Repository(ProjectStructureContext context)
        {
            _context = context;
        }
        public void Create(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void Delete(object id)
        {
            T entity = _context.Set<T>().Find(id);
            if (entity == null)
            {
                throw new NotFoundException(typeof(T).ToString());
            }
            Delete(entity);
        }

        public void Delete(T entity)
        {

            var dbSet = _context.Set<T>();

            if (_context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public IEnumerable<T> Get()
        {
            IQueryable<T> query = _context.Set<T>();
            if (!query.Any())
            {
                throw new ArgumentException();
            }
            return query.ToList();
        }

        public void Update(T entity)
        {
            if (_context.Set<T>().Find(entity) == null)
            {
                throw new NotFoundException(typeof(T).ToString());
            }
            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
