﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.Common.Models.DTO;
using ProjectStructure.Common.Models.StructureModels;

namespace ProjectStructure.BL.Interfaces
{
    public interface IQueriesService
    {
        Dictionary<string, int> GetTasksByUser(int id);
        IEnumerable<TaskDTO> GetTasksByUserNameCondition(int id);
        IEnumerable<TaskDTO> GetTasksByUserCurrentYear(int id);
        IEnumerable<(int, string, List<UserDTO>)> GetTeamsOrderTenYears();
        IEnumerable<UserDTO> GetUsersSortedByNameAndTasks();
        LastProjectCountAndLongerTasks GetLastProjectCountAndLongerTasks(int userId);
        IEnumerable<ProjectLongestAndShortestTaskAndUsersAmount> GetProjectLogestAndShortestTaskAndUsersAmount();
    }
}
